# README

# REQUISITOS
O desafio consiste em criar um site responsivo para clientes poderem reservar a compra de bonecos minions em miniatura. O intuito é que seja um site simples, basicamente uma landing page com a descrição do produto e um formulário para o cliente fazer a reserva do(s) seu(s) minions. Quando a reserva for feita, um email deve ser disparado para miguel@inventosdigitais.com.br com os dados preenchidos. Fique a vontade para pensar no texto e no layout.

# EXIGÊNCIAS
Para desenvolver, é preciso utilizar as seguintes tecnologias:

Ruby on Rails
CoffeeScript
Sass
Slim
Bootstrap

# SUGESTÕES
Além disso, algumas gems (bibliotecas) do Rails que provavelmente serão interessantes usar:

bootstrap-sass - Para incluir o bootstrap em formato sass
slim-rails - Para o slim
formtastic - Para facilitar a criação de formulários
devise - Para facilitar no cadastro e login de usuários