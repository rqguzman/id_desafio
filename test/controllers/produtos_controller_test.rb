require 'test_helper'

class ProdutosControllerTest < ActionDispatch::IntegrationTest
  
  test "should get root" do
    get root_path
    assert_response :success  
  end

  test "should get produtos" do
    get produtos_path
    assert_response :success
  end

end
