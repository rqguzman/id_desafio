Rails.application.routes.draw do
  devise_for :users
  root 'paginas#home'
  get '/sobre', to: 'paginas#sobre'
  get '/produtos', to: 'produtos#produtos'
end
